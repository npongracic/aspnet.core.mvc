﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DemoApp.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;

namespace DemoApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            //var todo = HttpContext.Session.GetString("todo");
            var todos = HttpContext.Session.GetString("todos");

            return View(todos != null ? JsonConvert.DeserializeObject<Todo>(todos) : null);
        }

        public IActionResult Privacy()
        {

            return View();
        }

        [HttpPost]
        public IActionResult AddTodo(Todo todo)
        {
            var listJson = HttpContext.Session.GetString("todos");
            if(listJson != null)
            {
                var list = JsonConvert.DeserializeObject<List<Todo>>(listJson);
                list.Add(todo);

                HttpContext.Session.SetString("todos", JsonConvert.SerializeObject(list));
            }
  

            // Dodaj podatke u bazu
            return RedirectToAction(nameof(Index));
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
